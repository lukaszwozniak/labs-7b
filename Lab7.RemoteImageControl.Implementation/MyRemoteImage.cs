﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7.RemoteImageControl.Implementation
{
    public class MyRemoteImage : IRemoteImage
    {
        private readonly IAddress AddressControl;
        private System.Windows.Controls.Control control;

        public MyRemoteImage(IAddress addressControl)
        {
            this.AddressControl = addressControl;
            this.control = new System.Windows.Controls.Control();
        }

        public System.Windows.Controls.Control Control
        {
            get { return control; }
        }

        public void Load(string url)
        {
            throw new NotImplementedException();
        }
    }
}
